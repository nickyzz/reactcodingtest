import checkForWin from '../src/js/fruitMachine/core/checkForWin';
import spin from '../src/js/fruitMachine/core/spin';

describe('Check For Winner', () => {
  test('different colors', () => {
    const arr = ['green', 'red', 'blue', 'yellow'];
    expect(checkForWin(arr)).toBe(false);
  });

  test('same color', () => {
    const arr = ['green', 'green', 'green', 'green'];
    expect(checkForWin(arr)).toBe(true);
  })
});


