// This component renders the players wins

import React from 'react';

type Props = {
  nrWins: number
}

export default ({ nrWins }: Props) => <h3 className='winsCounter'>Your wins: { nrWins }</h3>;
