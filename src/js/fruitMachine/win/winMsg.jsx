// Component that displays a message when there is a winner

import React from 'react';


type Props = {
  msg: string,
  onClick: Function
}


export default ({ onClick, msg }: Props) => (
  <button
    className='overlayWinMsg'
    onClick={ () => onClick() }
  >
    <h1 className='winMsg' >
      { msg }
    </h1>
  </button>
);
