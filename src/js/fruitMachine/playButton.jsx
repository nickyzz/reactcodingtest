// Component that triggers the spinning

import React from 'react';

type Props = {
  onClick: Function,
  label: string
}

export default ({ onClick, label }: Props) => (
  <button
    className='playButton'
    onClick={ () => onClick() }
  >
    { label }
  </button>
);
