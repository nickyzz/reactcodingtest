import React, { Component } from 'react';
import range from 'lodash/range';

import Wheel from './wheel/';
import PlayButton from './playButton';
import WinMsg from './win/winMsg';
import WinsCounter from './win/winsCounter';

import spin from './core/spin';
import checkForWin from './core/checkForWin';

class FruitMachine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colors: this.props.colors || ['red', 'blue', 'green', 'yellow'], // Give the posibility to add more colors.
      nrWheels: this.props.nrWheels || 3, // Give the posibility to add more wheels.
      spinnedColors: [], // The colors array after it's been randomized.
                        // The length of this array will always = nrWheels
                        // since each element represents a color rendered by a wheel.
      winner: false, // true if all the colors in spinnedColors array match;
                    // When is set to true the winning message will appear.
      nrWins: 0,  // Count how many times did the player won
    };
    // Bind functions that will be passed to children components
    this.startSpin = this.startSpin.bind(this);
    this.closeWinMsg = this.closeWinMsg.bind(this);
  }

  // This function does 3 things:
  // 1. Calls the spin function and updates the state with the result
  // 2. Calls the checkForWin function and updates the state with the result
  // 3. If there is a winner increment the wins counter and update the state
  startSpin() {
    const { nrWheels, colors, nrWins } = this.state;
    const spinnedColors = spin(nrWheels, colors);
    const winner = checkForWin(spinnedColors);
    const nrWinsCounter = winner === true ? nrWins + 1 : nrWins;
    this.setState({
      spinnedColors,
      winner,
      nrWins: nrWinsCounter,
    });
  }

  // Function that closes the win message and let's the player replay
  closeWinMsg() {
    this.setState({ winner: false });
  }

  props: {
    colors: ?Array<mixed>,
    nrWheels: ?number
  }

  render() {
    const { nrWheels, spinnedColors, winner, nrWins } = this.state;
    return (
      <div>
        <WinsCounter nrWins={ nrWins } />
        <div className='wheelContainer'>
          {
            // Make an array with the length = nrWheels and renders a Wheel component for each element.
            // Each element index represents it's color in the spinnedColors array.
            range(nrWheels).map(t => (
              <Wheel
                key={ t }
                color={ spinnedColors[t] }
                wheelNr={ t }
              />
            ))
          }
        </div>
        <PlayButton
          label='Play!'
          onClick={ this.startSpin }
        />
        {
          // If there is a winner render the WinMsg component
          winner &&
            <WinMsg
              msg='You Won!!!'
              onClick={ this.closeWinMsg }
            />
        }
      </div>
    );
  }
}

export default FruitMachine;
