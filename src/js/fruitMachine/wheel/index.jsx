// Component that renders either Color component either InitiamMsg component
// depending on the color prop.

import React from 'react';

import InitialMsg from './initialMsg';
import Color from './color';

type Props = {
  color: string,
  wheelNr: number
}

export default ({ color, wheelNr }: Props) => (
  <div className='wheel'>
    {
      color ?
        <Color color={ color } />
      :
        <InitialMsg msg={ `This is wheel number ${ wheelNr + 1 }.` } />
    }
  </div>
);