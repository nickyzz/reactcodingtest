// Component that renders a message before first spin

import React from 'react';

type Props = {
  msg: string
}

export default ({ msg }: Props) => (
  <div className='initialMsg' >
    { msg }
  </div>
);
