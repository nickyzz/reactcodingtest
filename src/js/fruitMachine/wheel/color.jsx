// Component that renders a box with the background set as the color prop

import React from 'react';

type Props = {
  color: string
}

export default ({ color }: Props) => (
  <div
    style={ { background: color } }
    className='color'
  >
    { color }
  </div>
);
