// This is the function that is responsible for the color randomize

import range from 'lodash/range';

export default (nrWheels, colors) =>
    // Generate an array with length = nrWheels.
    // Each element in the array represents a wheel.
    // Reduce the array of wheels so that each wheel should be replaced by a color
    range(nrWheels).reduce((acc) => {
      return [
        ...acc,
        // Select a random color and add it to the new array
        colors[Math.floor(Math.random() * colors.length)],
      ];
    }, []);
