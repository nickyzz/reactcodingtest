// This is the function that checks if all the colors are the same.
// It recievs an array of colors and compares the first element with the rest of them.

export default (arr) => {
  const firstElement = arr[0];
  let i;

  for (i = 0; i < arr.length; i++) {
    if (firstElement !== arr[i]) return false;
  }

  return true;
};
