import React, { Component } from 'react';

import FruitMachine from '../fruitMachine';

class App extends Component {
  render() {
    return (
      <div>
        <h1 className='title'>
          FRUIT MACHINE
        </h1>
        {/* Try to add nrWheels and colors as props to see what's happening. Like this for example: */}
        {/* <FruitMachine nrWheels={ 4 } colors={ ['red', 'green', 'purple', 'pink', 'brown', 'limegreen'] } /> */}
        <FruitMachine />
      </div>
    );
  }
}

export default App;
